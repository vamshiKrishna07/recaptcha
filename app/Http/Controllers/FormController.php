<?php

namespace App\Http\Controllers;

use App\Models\FormUpload;
use Illuminate\Http\Request;

class FormController extends Controller
{
    //

    public function form()
    {
        return view('formUpload');
    }

    public function formUpload(Request $request)
    {
        // validation
        $request->validate([
            'name' => 'required',
        ]);
        // form submission
        $formDetails = $request->all();
        FormUpload::create($formDetails);

        return redirect()->route('form');
    }
}
